from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.feature_extraction.text import CountVectorizer
from nltk.tokenize import RegexpTokenizer
import pandas as pd

# A toy dataset with just a few phrases
dataset = [
    "nice movie",
    "the protagonist was not good",
    "good scenario",
    "good scenario, fantastic acting, genius directing ",
]

# Tokenizer. We have used the same in the last lecture
tokenizer = RegexpTokenizer(r'[a-zA-Z0-9]+')

# Instantiate a CountVectorizer
# Same parameters/setting as in the last lecture (tokenizer, stowords removal, n-gram range, lowercase)
countVectorizer=CountVectorizer(lowercase=True, stop_words='english', ngram_range=(1, 1), tokenizer=tokenizer.tokenize)

# Vectorize the toy dataset
counts = countVectorizer.fit_transform(dataset)

# print counts
df = pd.DataFrame(data = counts.toarray(),columns = countVectorizer.get_feature_names())
print (df)

# Instantiate a TfidfVectorizer
# Same parameters/setting as in the last lecture (tokenizer, stowords removal, n-gram range, lowercase)
tfIdfVectorizer=TfidfVectorizer(lowercase=True, stop_words='english', ngram_range=(1, 1), tokenizer=tokenizer.tokenize)

# Vectorize the toy dataset
tfIdf = tfIdfVectorizer.fit_transform(dataset)

# print feature names
print('feature names')
print(tfIdfVectorizer.get_feature_names())
# print idf scores
print('idf scores')
print(tfIdfVectorizer.idf_)

# print feature name idf score side by side
print("{0:30}{1:30}".format("Feature","IDF"))
print('------------------------------------------------------------------------------------------------------------')
for i in range(len(tfIdfVectorizer.get_feature_names())):
    print ("{0:30}{1:30}".format(str(tfIdfVectorizer.get_feature_names()[i]), str(tfIdfVectorizer.idf_[i])))

# print TF-IDF values
df = pd.DataFrame(data = tfIdf.toarray(),columns = tfIdfVectorizer.get_feature_names())
print (df)
