# for splitting into 5 parts
from sklearn.model_selection import KFold
kf5 = KFold(n_splits=5, shuffle=False)

from dataset import get_column_from_tsv_data

# load phrase Ids
src_filename = "C:/Users/galanisd/Desktop/LangTech/sentiment-analysis-on-movie-reviews/__train.tsv"
train_filename = "C:/Users/galanisd/Desktop/LangTech/sentiment-analysis-on-movie-reviews/__train.tsv.split"
test_filename = "C:/Users/galanisd/Desktop/LangTech/sentiment-analysis-on-movie-reviews/__test.tsv.split"

phrases_ids = get_column_from_tsv_data(src_filename, 'PhraseId')
print(len(phrases_ids))


split_index=0

# split and for each generated split
for train_part, test_part in kf5.split(phrases_ids):
    # print objects that represent train and test part
    print('train: %s, test: %s' % (train_part, test_part))

    # open train and test file
    src = open(src_filename, "r")
    train_file = open(train_filename + str(split_index), "w")
    test_file = open(test_filename + str(split_index), "w")

    # copy header (first line)
    header = src.readline()
    train_file.write(header)
    test_file.write(header)

    current = 0;

    # read second line
    next_line = src.readline()

    # test_part is an array of indices
    # get the last one
    last = len(test_part) - 1
    # print first and last
    print('start: %s, end: %s' % (test_part[0], test_part[last]))

    while next_line:
        # if current line index is between first and last put
        # it in the test set
        if current >= test_part[0] and current <= test_part[last]:
            test_file.write(next_line)
            test_file.flush()
        else:
        # otherwise is for training
        # and put it there
            train_file.write(next_line)
            train_file.flush()

        # go to next line
        current = current + 1
        next_line = src.readline()

    # close new test/train files
    test_file.flush()
    train_file.flush()
    test_file.close()
    train_file.close()
    # got to next split
    split_index = split_index + 1